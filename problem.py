
import math

def num_of_divisor(num): // 20
	count = 0
	for i in range(1, (int)(math.sqrt(num)) + 1): // 1,2,3,4
		if num%i == 0:
			if num/i == i:
				count = count + 1

			else:	// 4 * 5
				count = count + 2

	return count

def find_8_divisor(num):
	flag = 0
	for i in range(1,num  + 1):
		count = 0
		count = num_of_divisor(i)
		if count == 8:
			flag = flag + 1

	return flag

		


total = find_8_divisor(1012)

print(total)